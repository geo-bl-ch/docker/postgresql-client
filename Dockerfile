FROM alpine:3.15

LABEL maintainer="Karsten Deininger <karsten.deininger@bl.ch>"

ENV LANG="C.UTF-8" \
    PGHOST="localhost" \
    PGPORT="5432" \
    PGUSER="postgres" \
    PGPASSWORD="password" \
    PGDATABASE="postgres" \
    PGCHECK="SELECT 1;" \
    PGTIMEOUT=60

RUN apk --update add bash postgresql-client && \
    adduser -u 1001 -G root -s /bin/bash -D client && \
    chgrp -R 0 /etc/passwd && \
    chmod -R g=u /etc/passwd && \
    mkdir /data && \
    chgrp -R 0 /data && \
    chmod -R g=u /data && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/
COPY --chown=1001:0 wait_for_db /usr/local/bin/

USER 1001

WORKDIR /data

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["/bin/bash"]
